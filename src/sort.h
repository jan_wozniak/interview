#ifndef _SORT_H
#define _SORT_H

#include <cstdlib>
#include <string.h>

#include "utils.h"

void print(int ptr[], int len);

template<typename T>
void split(const ArrWrap<T> original, ArrWrap<T> &left, ArrWrap<T> &right) {
    size_t half = original.len / 2;
    left = ArrWrap<T>(original.ptr, half);
    right = ArrWrap<T>(original.ptr+half, original.len-half);
}

template<typename T>
void merge(const ArrWrap<T> left, const ArrWrap<T> right, ArrWrap<T> &merged) {
    size_t l_index = 0, r_index = 0, index = 0;
    merged.len = left.len + right.len;
    while(l_index < left.len && r_index < right.len){
        if(left.ptr[l_index] <= right.ptr[r_index]) 
            merged.ptr[index++] = left.ptr[l_index++];
        else
            merged.ptr[index++] = right.ptr[r_index++];
    }
    memcpy(merged.ptr+index, left.ptr+l_index, (left.len-l_index)*sizeof(T));
    memcpy(merged.ptr+index, right.ptr+r_index, (right.len-r_index)*sizeof(T));
}

template<typename T>
void merge_sort(ArrWrap<T> original, ArrWrap<T> &temp) {
    if(original.len > 1){
        ArrWrap<T> l, r, l_t, r_t;
        split(original, l, r);
        split(temp, l_t, r_t);
        merge_sort(l, l_t);
        merge_sort(r, r_t);
        merge(l_t, r_t, original);
        memcpy(temp.ptr, original.ptr, original.len*sizeof(T));
    }
}

template<typename T>
void merge_sort(T list[], size_t len){
    T* temp_arr = new T[len];
    memcpy(temp_arr, list, len*sizeof(T));
    ArrWrap<T> original(list, len), temp(temp_arr, len);

    merge_sort(original, temp);

    delete[] temp_arr;
}


#endif
