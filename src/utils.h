#ifndef _UTILS_H
#define _UTILS_H

#include <ctime>
#include <chrono>

template<typename T>
struct ArrWrap {
    ArrWrap(T* arr, size_t length) 
        : ptr(arr), len(length) {}
    ArrWrap() 
        : ptr(nullptr), len(0) {}
    T* ptr;
    size_t len;
};


size_t uniq_naive(int array[], size_t len);
size_t uniq_mergesort(int array[], size_t len);
size_t uniq_hash(int array[], size_t len);

template<typename ... Args>
int measure(void (*f)(Args ... args), Args ... args){
    using namespace std::chrono;
    auto start = high_resolution_clock::now();
    f(args...); 
    auto end = high_resolution_clock::now();
    return duration_cast<milliseconds>(end - start).count();
}

template<typename Return, typename ... Args>
int measure(Return &ret, Return (*f)(Args ... args), Args ... args){
    using namespace std::chrono;
    auto start = high_resolution_clock::now();
    ret = f(args...); 
    auto end = high_resolution_clock::now();
    return duration_cast<milliseconds>(end - start).count();
}

#endif
