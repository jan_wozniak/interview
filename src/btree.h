#ifndef _TREE_H
#define _TREE_H

#include <iostream>
#include <stack>
#include <queue>

#include "utils.h"

enum Type {
    BREADTH_FIRST,
    SORTED_ARRAY,
    SEARCH_TREE 
};

enum Traversal {
    IN_ORDER,
    BREADTH_ORDER
};

template<typename T> struct BTreeNode;

template<typename T>
struct BTreeIter {
    BTreeNode<T> *root;
    BTreeNode<T> *current;
    std::stack<BTreeNode<T>*> stack;
    std::queue<BTreeNode<T>*> queue;
    Traversal method;

    BTreeIter(BTreeNode<T> *tree, Traversal traversal_method) 
        : root(tree), current(tree), method(traversal_method) {}
    bool operator!=(const BTreeIter<T> &x){
        return current!=x.current;
    }
    
    void preorder_next() {
        if(current->left){
            if(current->right)
                stack.push(current->right);
            current = current->left;
        } else if (!stack.empty()){
            current = stack.top();
            stack.pop();
        } else {
            current=nullptr;
        }
    }
    
    void breadth_next() {
        if(current->left)
            queue.push(current->left);
        if(current->right)
            queue.push(current->right);
        if(!queue.empty()){
            current = queue.front();
            queue.pop();
        } else {
            current=nullptr;
        }
    }

    BTreeIter<T>& operator++() {
        switch(method) {
            case IN_ORDER:
                preorder_next();
                break;
            case BREADTH_ORDER:
                breadth_next();
                break;
        }
        return *this;
    }

    T operator*() const {
        return current->val;
    }
};

template<typename T>
struct BTreeNode {
    T val;
    BTreeNode<T> *left = nullptr;
    BTreeNode<T> *right = nullptr;
};

template<typename T>
struct BTree {
    BTreeNode<T> *root = nullptr;
    Traversal traversal_method;
    
    void bfs(std::initializer_list<T> array) {
        std::queue<BTreeNode<T>**> queue;

        root = new BTreeNode<T>();
        queue.push(&root);

        BTreeNode<T> **ptr;
        for(T i: array){
            ptr = queue.front();
            queue.pop();
            if(*ptr){ //first exists
                (*ptr)->val = i;
            } else { //others are nullptr
                (*ptr) = new BTreeNode<T>();
                (*ptr)->val = i;
            }
            queue.push(&(*ptr)->left);
            queue.push(&(*ptr)->right);
        }
    }
    
    void sorted(const ArrWrap<T> arr, BTreeNode<T>** local_root) {
        if(arr.len > 0){
            size_t half = arr.len / 2; 
            *local_root = new BTreeNode<T>();
            (*local_root)->val = arr.ptr[half];
            sorted(ArrWrap<T>(arr.ptr, half), &(*local_root)->left);
            sorted(ArrWrap<T>(arr.ptr+half+1, arr.len-half-1), &(*local_root)->right);
        }
    }

    void sorted(std::initializer_list<T> array) {
        T *temp_array = new T[array.size()];
        int offset = 0;
        for(T x: array){
            temp_array[offset++] = x;
        }
        sorted(ArrWrap<T>(temp_array, array.size()), &root);

        delete[] temp_array;
    }
    
    BTree(std::initializer_list<T> a, Type t=SORTED_ARRAY, Traversal tm=BREADTH_ORDER)
            : traversal_method(tm) {
        switch (t) {
            case BREADTH_FIRST: 
                bfs(a);
                break;
            case SORTED_ARRAY:
                sorted(a);
                break;
            case SEARCH_TREE:
                break;
        }
    }

    BTree(BTreeNode<T> *n) : root(n) {}

    int height() {
        if(root == nullptr)
            return 0;
        return std::max(1+BTree(root->left).height(), 1+BTree(root->right).height());
    }

    void print() {

    }
    
    BTreeIter<T> begin() { return BTreeIter<T>(root, traversal_method); };
    BTreeIter<T> end()   { return BTreeIter<T>(nullptr, traversal_method); };
};

#endif
