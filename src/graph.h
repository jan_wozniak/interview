#ifndef _GRAPH_H
#define _GRAPH_H

#include <iostream>
#include <vector>
#include <set>

#define UNDIRECTED 0x0
#define DIRECTED   0x1

//forward declaration for Node
template<typename EdgeT, typename NodeT> struct Edge;

template<typename EdgeT, typename NodeT> 
struct EdgeIter {
    int index;
    std::vector<Edge<EdgeT, NodeT>*> *edges;
    
    
    EdgeIter(std::vector<Edge<EdgeT, NodeT>*> *e) : index(0) {
        if(e && e->size() == 0) edges = nullptr;
        else edges = e;
    }   

    bool operator!=(const EdgeIter<EdgeT, NodeT> &x) {
        return !((index == x.index) && (edges == x.edges));
    }
    EdgeIter<EdgeT, NodeT>& operator++() {
        index++;
        if(index >= edges->size()){
            index = 0;
            edges = nullptr;
        }
        return *this;
    }
    Edge<EdgeT, NodeT> operator*() {
        return (*(*edges)[index]);
    }
};

template<typename EdgeT, typename NodeT>
struct Node {
    NodeT label;
    std::vector<Edge<EdgeT, NodeT>*> edges;

    Node(NodeT v) : label(v) {}
    
    EdgeIter<EdgeT, NodeT> begin() { return EdgeIter<EdgeT, NodeT>(&edges); }
    EdgeIter<EdgeT, NodeT> end()   { return EdgeIter<EdgeT, NodeT>(nullptr); }
};

template<typename EdgeT, typename NodeT>
struct Edge {
    EdgeT val;
    Node<EdgeT, NodeT>* src;
    Node<EdgeT, NodeT>* dst;

    Edge(EdgeT v, Node<EdgeT, NodeT>* s, Node<EdgeT, NodeT>* d) :
        val(v), src(s), dst(d) {};
};

template<typename EdgeT, typename NodeT> 
struct NodeIter {
    int index;
    std::vector<Node<EdgeT, NodeT>> *nodes;

    NodeIter(std::vector<Node<EdgeT, NodeT>> *n) : index(0) {
        if(n && n->size() == 0) nodes = nullptr;
        else nodes = n;
    }

    bool operator!=(const NodeIter<EdgeT, NodeT> &x) {
        return !((index == x.index) && (nodes == x.nodes));
    }
    NodeIter<EdgeT, NodeT>& operator++() {
        index++;
        if(index >= nodes->size()){
            index = 0;
            nodes = nullptr;
        }
        return *this;
    }
    Node<EdgeT, NodeT> operator*() {
        return (*nodes)[index];
    }
};

template<typename EdgeT, typename NodeT>
struct Graph {
    std::vector<Node<EdgeT, NodeT>> nodes;
    std::vector<Edge<EdgeT, NodeT>*> edges;
    unsigned int type;

    NodeIter<EdgeT, NodeT> begin() { return NodeIter<EdgeT, NodeT>(&nodes); }
    NodeIter<EdgeT, NodeT> end()   { return NodeIter<EdgeT, NodeT>(nullptr); }

    Graph(unsigned int t) : type(t) {};

    void add_node(Node<EdgeT, NodeT> *node){
        nodes.push_back(*node);
    }
    void add_edge(Edge<EdgeT, NodeT> *edge) {
        edges.push_back(edge);
        edge->src->edges.push_back(edge);
        if(!(type & DIRECTED))
            edge->dst->edges.push_back(edge);
    }
};

//Both edges and nodes can repeat
template<typename EdgeT, typename NodeT>
struct Walk {
};

//Nodes can repeat, edges not
template<typename EdgeT, typename NodeT>
struct Trail {
};

//Neither edges nor nodes can repeat
template<typename EdgeT, typename NodeT>
struct Path {
    std::vector<Node<EdgeT, NodeT>*> node_sequence;
    std::set<Node<EdgeT, NodeT>*> node_set;

    int add_node(Node<EdgeT, NodeT>* n){
        return 0; 
    }
};


void make_test_graph(Graph<int, char> &g);



#endif
