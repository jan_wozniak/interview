#include <iostream>
#include <map>
#include <algorithm>

#include "utils.h"
#include "sort.h"

size_t uniq_naive(int array[], size_t len) {
    int* temp_arr = new int[len];
    size_t offset, temp_pos = 0;
    bool duplicit;

    for(size_t i=0; i<len; i++){
        offset = 0;
        duplicit = false;
        while(offset<i && !duplicit){ 
            if(array[i] == array[offset])
                duplicit = true;
            offset++;
        }
        if(!duplicit)
            temp_arr[temp_pos++] = array[i];
    }

    temp_arr[temp_pos] = '\0';
    memcpy(array, temp_arr, temp_pos*sizeof(int));
    delete[] temp_arr;
    return (size_t)temp_pos;
}

void uniq_merge(int arr[], int indexes[], size_t start, size_t center, size_t end){
    /*size_t l_index = start, r_index = center;
    while(l_index<center && r_index<end) {
        if(arr[indexes[l_index]] < arr[indexes[r_index]]){
            while((++l_index < center) && (indexes[l_index] >= 0));
        }
        else if(arr[indexes[l_index]] > arr[indexes[r_index]])
            r_index++;

    }*/
}

void uniq_mergesort(int arr[], int indexes[], size_t start, size_t end) {
    if((end-start) > 1){
        size_t center = (end-start)/2;
        uniq_mergesort(arr, indexes, start, center);
        uniq_mergesort(arr, indexes, center, end);
        uniq_merge(arr, indexes, start, center, end);
    }
}

void uniq_mergefilter(int arr[], int indexes[], size_t len){
}

size_t uniq_mergesort(int array[], size_t len) {
    int* temp = new int[len];
    for(size_t i=0; i<len; i++) temp[i] = i;
    
    uniq_mergesort(array, temp, 0, len);
    uniq_mergefilter(array, temp, len);

    return len;
}

size_t uniq_hash(int array[], size_t len) {
    int* temp_arr = new int[len];
    size_t new_index = 0;
    std::map<int, bool> duplicit;    
    
    for(size_t i=0; i<len; i++){
        if( !duplicit[array[i]] ) {
            temp_arr[new_index++] = array[i];
            duplicit[array[i]] = true;
        }
    }

    memcpy(array, temp_arr, len*sizeof(int));
    delete[] temp_arr;
    return new_index;
}
