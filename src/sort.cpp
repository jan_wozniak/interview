#include <iostream>
#include <cstring>

#include "sort.h"

void print(int ptr[], int len){
    for(int i=0; i<len; i++){
        std::cout << ptr[i] << " ";
    }
    std::cout << std::endl;
}
