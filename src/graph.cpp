#include "graph.h"

void make_test_graph(Graph<int, char> &g) {
    for(char val='a'; val<'e'; val++){
        g.add_node(new Node<int, char>(val));
    }
    g.add_edge(new Edge<int, char>(2, &(g.nodes[0]), &(g.nodes[1]))); 
    g.add_edge(new Edge<int, char>(3, &(g.nodes[1]), &(g.nodes[3]))); 
    g.add_edge(new Edge<int, char>(1, &(g.nodes[2]), &(g.nodes[3]))); 
    g.add_edge(new Edge<int, char>(1, &(g.nodes[1]), &(g.nodes[2]))); 
    g.add_edge(new Edge<int, char>(0, &(g.nodes[3]), &(g.nodes[1]))); 
}
