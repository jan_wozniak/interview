#ifndef _LIST_H
#define _LIST_H

#include <iostream>


template<typename T>
struct LItem {
    T data;
    LItem *next;

    LItem(T val) : data(val), next(nullptr) {};
};

template<typename T>
struct LIterator {
    LItem<T> *ptr;

    LIterator(LItem<T> *item) 
        : ptr(item) {};
    bool operator!=(const LIterator<T> &x) {
        return ptr!=x.ptr;
    };
    LIterator<T>& operator++() {
        ptr = ptr->next;
        return *this;
    };
    T operator*() const { 
        return ptr->data; 
    };
};

template<typename T>
struct List {
    LItem<T> *first = nullptr;
    LItem<T> *last = nullptr;

    List(std::initializer_list<T> array) : first(nullptr), last(nullptr) {
        for(T val: array){
            LItem<T> *new_item = new LItem<T>(val);
            if(last == nullptr) {
                last = new_item;
                first = last;
            } else {
                last->next = new_item;
                last = last->next;
            }
        }
    };
    LIterator<T> begin() { return LIterator<T>(first); };
    LIterator<T> end()   { return LIterator<T>(nullptr); };
};

template<typename T>
struct LlItem {
    List<T> *list;
    LlItem<T> *next;

    LlItem(std::initializer_list<T> array) : next(nullptr){
        list = new List<T>(array);
    };
    
};

template<typename T>
struct LlIter {
    LItem<T> *ptr;
    LlItem<T> *l_ptr;

    LlIter(LItem<T> *item, LlItem<T> *LItem) 
        : ptr(item), l_ptr(LItem) {};
    bool operator!=(const LlIter<T> &x) {
        return ptr!=x.ptr;
    };
    LlIter<T>& operator++() {
        if(ptr->next) {
            ptr = ptr->next;
        } else {
            l_ptr = l_ptr->next;
            if(l_ptr)
                ptr = l_ptr->list->first;
            else
                ptr = nullptr;
        }
        return *this;
    };
    T operator*() { return ptr->data; };
};

template<typename T>
struct ListOfLists {
    LlItem<T> *first = nullptr;
    LlItem<T> *last = nullptr;

    ListOfLists(std::initializer_list<std::initializer_list<T>> list) 
        : first(nullptr), last(nullptr) {
        for(auto array: list){
            LlItem<T> *new_item = new LlItem<T>(array);
            if(last == nullptr){
                last = new_item;
                first = last;
            } else {
                last->next = new_item;
                last = last->next;
            }
        }
    };

    int a = 1;
    int b = 2;
    LlIter<T> begin() { return LlIter<T>(first->list->first, first); };
    LlIter<T> end()   { return LlIter<T>(nullptr, nullptr); };
};

#endif
