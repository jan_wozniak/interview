#include "fizzbuzz.h"
#include <iostream>

using namespace std;

bool buzz_func(int i, int buzz, const char* buzz_word){
    if((i % buzz) == 0){
        cout << buzz_word;
        return true;
    }
    return false;
}

void fizzbuzz(int max_number, int buzz[], const char* buzz_word[], int len){
    bool buzzed;
    for(int i=1; i<=max_number; i++){
        buzzed = false;
        for(int b=0; b<len; b++){
           buzzed |= buzz_func(i, buzz[b], buzz_word[b]); 
        }
        if( !buzzed ) cout << i;
        cout << endl;
    }
}
