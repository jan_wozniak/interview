class Node
    attr_accessor :left
    attr_accessor :right
    attr_accessor :parent
    attr_accessor :val

    def initialize(parent)
        @val = nil
        @parent = parent
    end
    def set(val)
        @left = Node.new(self)
        @right= Node.new(self)
        @val = val
    end
    def delete()
        @left=nil
        @right=nil
        @val=nil
    end
end

class BinaryHeap
    def initialize()
        @root  = Node.new(nil)
        @empty = [@root]
    end

    def add_val(val)
        current = @empty[0]
        current.set(val)
        @empty << current.left << current.right
        @empty.delete_at(0)
        shift_up(current)
    end

    def shift_up(node)
        if node.parent
            if(node.val > node.parent.val)
                node.val, node.parent.val = node.parent.val, node.val
                shift_up(node.parent)
            end
        end
    end

    def sift_down(node)
        arr = []
        arr << node.left if node.left.val != nil
        arr << node.right if node.right.val != nil
        m, max_node = arr.map{|x| [x.val, x]}.max{|a,b| a[0] <=> b[0]}
        if max_node.val > node.val
            max_node.val, node.val = node.val, max_node.val
            sift_down(max_node)
        end
    end

    def last_elem(depth, width, node)
        if node.val != nil
            return [last_elem(depth+1, width, node.left), 
                    last_elem(depth+1, width+1, node.right)].max{|a,b| a[0..1] <=> b[0..1]}
        else
            return [depth, width, node.parent]
        end
    end

    def pop()
        val = @root.val
        depth, w, last = last_elem(0, 0, @root)
        @empty.delete(last.left)
        @empty.delete(last.right)
        @empty.unshift(last)
        @root.val = last.val
        sift_down(@root)
        last.delete
        return val
    end

    def print()
        level = [@root]
        puts ""
        while not level.empty?
            puts level.map {|n| n.val}.join(" ")
            level = level.inject([]) {|acc, n| 
                acc << n.left if n.left.val != nil
                acc << n.right if n.right.val != nil
                acc
            } 
        end
    end
end

class ArrayBinaryHeap
    def initialize()
        @array = []
    end

    def parent(i)
        i == 0 ? -1 : ((i-1)/2.0).floor
    end

    def left(i)
        i*2+1
    end

    def right(i)
        i*2+2
    end

    def shift_up(i)
        pi = parent(i)
        while pi >= 0 && @array[i] > @array[pi]
            @array[i], @array[pi] = @array[pi], @array[i] 
            i = pi
            pi = parent(i)
        end
    end

    def shift_down(i)
        l = left(i)
        r = right(i)
        arr = []
        arr << l if l < @array.size
        arr << r if r < @array.size
        max, max_i = arr.map{|x| [@array[x], x]}.max
        while max && @array[max_i] > @array[i]
            @array[i], @array[max_i] = @array[max_i], @array[i]
            i = max_i
            
            l = left(i)
            r = right(i)
            arr = []
            arr << l if l < @array.size
            arr << r if r < @array.size
            max, max_i = arr.map{|x| [@array[x], x]}.max
        end
    end

    def add_val(val)
        @array << val
        shift_up(@array.size-1)
    end

    def pop()
        @array[0] = @array[-1]
        @array.delete_at(-1)
        shift_down(0)
    end

    def print()
        level = [0]
        puts ""
        while not level.empty?
            puts level.map { |i| @array[i] }.join(" ")
            level = level.inject([]) {|acc, i|
                acc << left(i) if left(i) < @array.size 
                acc << right(i) if right(i) < @array.size 
                acc
            }
        end
    end
end

h = ArrayBinaryHeap.new()
h.add_val(1)
h.add_val(2)
h.add_val(3)
h.add_val(4)
h.add_val(5)
h.add_val(6)
h.print
h.pop
h.print
h.add_val(6)
h.add_val(7)
h.print
