class Node
    attr_accessor :key
    attr_accessor :edges

    def initialize(key)
        @edges = Array.new
        @key = key
    end
    def add_edge(edge)
        @edges << edge
    end
end

class Edge
    attr_reader :src
    attr_reader :dst
    attr_accessor :val

    def initialize(src, dst, val)
        @src = src
        @dst = dst
        @val = val
    end
    
    def get_reverse()
        return Edge.new(@dst, @src, val)
    end
end

class Graph
    attr_reader :nodes
    attr_reader :edges

    def initialize()
        @nodes = Hash.new
        @edges = Array.new
    end

    def add_node(node)
        @nodes[node.key] = node
    end

    def add_direct_edge(edge) 
        @nodes[edge.src].add_edge(edge)
        @edges << edge
    end

    def add_edge(edge)
        reversed = edge.get_reverse()
        @nodes[edge.src].add_edge(edge)
        @nodes[edge.dst].add_edge(reversed)
        @edges << edge
        @edges << reversed
    end

    def write_out()
        @nodes.each{ |key, node|
            puts "\n#{key}"
            node.edges.each{ |e|
                puts "#{e.src} -#{e.val}-> #{e.dst}"
            }
        }
    end
end

#shortest path tree, non-negative edges
#without fibonacci heap runs in O(|V|^2), with in O(|E| + |V|*log(|V|))
def dijkstra(graph, initial_node)
    dist = graph.nodes.keys.inject({}){|d,k| d[k] = Float::INFINITY; d}
    dist[initial_node] = 0
    prev = {initial_node => initial_node}
    unvisited = graph.nodes.keys

    while not unvisited.empty?
        temp_dist, top = unvisited.map{|x| [dist[x], x]}.min
        unvisited.delete(top)
        graph.nodes[top].edges.each {|e|
            dist[e.dst], prev[e.dst] = 
                [[dist[e.dst], prev[e.dst]], [e.val+temp_dist, e.src]].min
        }
    end

    return dist, prev
end

#shortest path tree, capable of negative cycle handling
#runs in O(|V|*|E|)
def bellman_ford(graph, initial_node)
    dist = graph.nodes.keys.inject({}){|d,k| d[k] = Float::INFINITY; d}
    dist[initial_node] = 0
    prev = {initial_node => initial_node}

    (1...graph.nodes.size).each { |i|
        (graph.edges).each { |edge|
            if dist[edge.src] + edge.val < dist[edge.dst]
                dist[edge.dst] = dist[edge.src]+edge.val
                prev[edge.dst] = edge.src
            end
        }
    }

    #negative check
    (graph.edges).each { |edge|
        if dist[edge.src] + edge.val < dist[edge.dst]
            return "has negative loop"
        end
    }

    return dist, prev 
end

#shortest path table and transitive closure
#runs in O(|V|^3)
def floyd_warshall(graph)
    dist = Hash.new{ |hash, k| hash[k] = Hash.new{Float::INFINITY} }
    graph.edges.each {|e| dist[e.src][e.dst]=e.val}
    graph.nodes.keys.each {|n| dist[n][n] = 0}
    graph.nodes.keys.each { |k|
        graph.nodes.keys.each { |i|
            graph.nodes.keys.each { |j|
                if dist[i][j] > dist[i][k]+dist[k][j]
                    dist[i][j] = dist[i][k]+dist[k][j]
                end
            }
        }
    }
    return dist
end

#my naive implementation of spanning tree
def spanning_tree(g)
    spanning_tree = Graph.new()
    g.nodes.each {|k, n| spanning_tree.add_node(Node.new(n.key))}
    sorted_edges = g.edges.map{|e| [e.val, e]}.sort{|a,b| a[0]<=>b[0]}.map{|e| e[1]}
    sorted_edges.each {|e|
        path = bellman_ford(spanning_tree, e.src)[0][e.dst]
        if path == Float::INFINITY
            spanning_tree.add_edge(e)
        end
    }
    spanning_tree
end

g = Graph.new()
g.add_node(Node.new(:a))
g.add_node(Node.new(:b))
g.add_node(Node.new(:c))
g.add_node(Node.new(:d))
g.add_node(Node.new(:e))

g.add_edge(Edge.new(:a, :b, 7))
g.add_edge(Edge.new(:a, :c, 2))
g.add_edge(Edge.new(:b, :d, 1))
g.add_edge(Edge.new(:c, :b, 1))
g.add_edge(Edge.new(:a, :d, 8))
g.add_edge(Edge.new(:a, :e, 8))

g.write_out()
puts "\nDijkstra ------------"
puts dijkstra(g, :a)
puts "\nBellman-Ford ------------"
puts bellman_ford(g, :a)
puts "\nFloyd-Warshall ------------"
puts floyd_warshall(g)
puts "\nspanning-tree ------------"
spanning_tree(g).write_out()
