TARGET  = app
SRC     = src
BIN     = bin
PARAM   = -std=c++0x -Wall -pedantic -O3

CC = g++

#lists according to source files
SOURCE  := $(wildcard $(SRC)/*.cpp)
BINS    := $(patsubst src%, bin%, $(SOURCE))  #temp variable
OBJECTS := $(BINS:.cpp=.o)                    #list of object files
DFILES  := $(BINS:.cpp=.d)                    #list of dependancy files

all: $(BIN)/$(TARGET)

################################################################################
#Main target.
$(BIN)/$(TARGET): dirs $(OBJECTS)
	@echo Linking $(TARGET) 
	@$(CC) $(PARAM) -o $(BIN)/$(TARGET) $(OBJECTS) 

#Rule for creating object file and .d file, the sed magic is to add
#the object path at the start of the file because the files gcc
#outputs assume it will be in the same dir as the source file.
$(BIN)/%.o: $(SRC)/%.cpp
	@echo Creating object file for $*.cpp
	@$(CC) $(PARAM) -Wp,-MMD,$(BIN)/$*.dd -c $< -o $@
	@sed -e '1s/^\(.*\)$$/$(subst /,\/,$(dir $@))\1/' $(BIN)/$*.dd > $(BIN)/$*.d
	@rm -f $(BIN)/$*.dd

#Empty rule to prevent problems when a header is deleted.
%.h: ;

#Cleans up the objects, .d files and executables.
clean:
	@echo Making clean.
	@rm -rf $(BIN)/*

run:
	cd $(BIN); ./$(TARGET)

#Create directories
dirs:
	@-if [ ! -e $(BIN) ]; then mkdir $(BIN); fi;

end:
	@echo Finished

#Includes the .d files 
-include $(DFILES)
